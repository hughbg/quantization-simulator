# README

Simulator for the quantization of the output of LWA-OVRO F engine

Change the file paths (to the .dat files) in the C++ code and compile it with:
```
g++ -O -std=c++11 gains_simulator.cc -o gains_simulator
```


Usage: gains\_simulator <scale> <num\_integrations>   setting   setting  ..... 

The first two arguments are required. "scale" is used to scale the data up or down (so set it to 1 for no scaling).
"num_integrations" is how many pretend time samples to integrate over. Each time sample is a quantized value.
This is done for all channels.

Multiple "settings" can be specified. A "gains" setting must be present. The settings are:

gains:flat

gains:lines

gains:curved

bandpass_flip:on  	[ or off ]

bandpass_flat:on	[ or off ]

random:gauss		[ or flip ]

saturation_check:on	[ or off ]

rounding:on		[ or off ]

The settings that simulate the quantization as implemented in the OVRO pipeline are: gains:curved random:gauss saturation_check:on rounding:on .

The output is the array written to file signal_equiv.dat. It covers the band, with a value for each channel.
