// Created by Hugh Garsden, 2017
// g++ -O -std=c++11 gains_simulator.cc -o gains_simulator

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include <random>

using namespace std;

const double scale_fac =  0.63025;		// from roach_config_leda_obs.py
const int shift_fac = 27;

const int NUM_LOWER_BITS = 31;		// The selected 4 bits will be NUM_LOWER_BITS+3:NUM_LOWER_BITS. Larry: 35:32, Danny: 34:31.

extern long int two_up(int x);     // Does 2**x

// ------------ ArrayFloat ----------------------------------------------------------------------------

// This is going to hold the gains and the bandpass. The bandpass is NOT data, it tells how the amplitude
// of the data should change over the band. The data itself consists of Gassian random varibales drawn
// from a distribution of mean=0, stdev=1, then scaled by the bandpass. This ArrayFloat class has several useful
// operations for doing things to the bandpass and gains.

class ArrayFloat {
public:
  double *data;
  int len;
  
  ArrayFloat() { len = 0; data = NULL; }
  ArrayFloat(const char *fname) { load_gains(fname); }
  ArrayFloat(int l) { data = new double [l]; len = l; for (int i=0; i<len; ++i) data[i] = 0; }
  ~ArrayFloat() { if ( data != NULL ) delete[] data; }
  void savetxt(const char *s);
  void flat_gains(int length);
  void load_gains(const char* filename);
  double& operator[](int i) { return data[i]; }
  void bp_setup(const ArrayFloat&);
  void flip();
  void flat();
  void scale(double scale);
  double mean();
};

// Load an array of equalizer gains, like the correlator uses. Multiply them by
// odata like in roach_config_leda_obs.py. odata is a big number calculated from
// scale_fac and shift_fac.
void ArrayFloat::load_gains(const char *fname) {
   std::fstream myfile(fname, std::ios_base::in);
   if ( !myfile ) {
	   cerr << "Failed to open " << fname << endl;
	   exit(1);
   }
   float a;
   len = 0;
   while ( myfile >> a ) len += 1;
   
   myfile.close();
  
   data = new double[len];
   
   std::fstream myfile_again(fname, std::ios_base::in);

   int i = 0;
   while ( myfile_again >> data[i] ) ++i;
   
   myfile_again.close();
   
   // The equalization gains need to be multiplied by the constant.
   double odata = (two_up(shift_fac)-1)*scale_fac;
   for (int i=0; i<len; ++i) data[i] *= odata;
}

// Don't generate equalizer gains, just use the value of odata (constant) for the gains.
void ArrayFloat::flat_gains(int length) {
  len = length;
  
  data = new double[len];
  double odata = (two_up(shift_fac)-1)*scale_fac;
  for (int i=0; i<len; ++i) data[i] = odata;
}

// Save to a file like numpy savetxt.
void ArrayFloat::savetxt(const char *fname) {
  ofstream datafile(fname);
  
  for (int i=0; i<len; ++i) datafile << data[i] << endl;
  datafile.close();
}

// Generate a bandpass amplitude over the band, that is the inverse of the gains, but
// scaled a bit.
void ArrayFloat::bp_setup(const ArrayFloat& gains) {
  len = gains.len;
  
  data = new double[len];
  for (int i=0; i<gains.len; ++i) { 
  	data[i] = 1e9/gains.data[i];
  }
}

// Invert the data, for example invert the bandpass or gains.
void ArrayFloat::flip() {
  double max=-1e39, min=1e39;
  for(int i=0; i<len; ++i) { 
	  if ( data[i] > max ) max = data[i]; 
	  if ( data[i] < min ) min = data[i];
  }
  for(int i=0; i<len; ++i) data[i] = max-data[i]+min;
} 

// Flatten the data so all values become the mean of the data.
void ArrayFloat::flat() {
  double mean = 0;
  for(int i=0; i<len; ++i) mean += data[i];
  mean /= len;
  for(int i=0; i<len; ++i) data[i] = mean;
}
  
// Scale the data.
void ArrayFloat::scale(double scale) {
	for (int i=0; i<len; ++i) data[i] *= scale;
}

// Find the mean of the data.
double ArrayFloat::mean() {
	double mean = 0;
	for(int i=0; i<len; ++i) mean += data[i];
	mean /= len;
	return mean;
}

// ----------------------ArrayInt64 --------------------------------------------------------

// A convenience class for arrays of long int.

class ArrayInt64 {
public:
  long int *data;
  int len;
  
  ArrayInt64() { len = 0; data = NULL; }
  ArrayInt64(int l) { data = new long int[l]; len = l; for (int i=0; i<len; ++i) data[i] = 0; }
  void savetxt(const char *s);
  long int& operator[](int i) { return data[i]; }
};

void ArrayInt64::savetxt(const char *fname) {
  ofstream datafile(fname);
  
  for (int i=0; i<len; ++i) datafile << data[i] << endl;
  datafile.close();
}




// --------------------- LongInt ------------------------------------------------------------

// Basically a long int (64b) but adds some useful functionality/

class LongInt {
public:
  long int val;
  
  LongInt() { val = 0; }
  LongInt(const LongInt& li) { val = li.val; }
  LongInt(double v) { val = (long int)round(v); }
  LongInt(long int v) { val = v; }
  string binary_str();
  void round_even();
  void trunc();
  LongInt operator*(const LongInt&);
  LongInt& operator=(const LongInt& li) { val = li.val; return *this; }
  void right_shift(int num) { val >>= num; }
};

// Print long int as a binary string.
string LongInt::binary_str() {
  char s[65];
 
  long int v = val;
  long int v1;
  for (int i=0; i<64; ++i) {
    v1 = v & 0b0000000000000000000000000000000000000000000000000000000000000001;
    if ( v1 == 1 ) s[64-i-1] = '1'; else s[64-i-1] = '0';
    v >>= 1;
  }
  s[64] = '\0';
  return string(s);
}

// Do round half to even on the long it, based on knowledge of which 4 bits are to be selected.
// That number is related to NUM_LOWER_BITS.
void LongInt::round_even() {
  // If the number is -ve then work on the +ve version. This screws up if the numbers are up near 2^64.
  bool neg = false;
   
  if ( val < 0 ) { 
    val = -val;
    neg = true;
  }

  // For rounding select the remainder which is bit NUM_LOWER_BITS-1 down.
  // This simple bit op would be messy if the number is -ve due to complements etc.
  // Suppose NUM_LOWER_BITS is 4 and we are operating on 8-bit numbers. Then
  // two_up(NUM_LOWER_BITS)-1   would be  00001111, thus it can be used to select the lower bits.
  long int remainder = val & (two_up(NUM_LOWER_BITS)-1);     // Bottom NUM_LOWER_BITS bits, eg. 31:0

  // Get the number that is in the top bits of x
  long int number = val >> NUM_LOWER_BITS;
  
  // Doing rounding. Suppose NUM_LOWER_BITS is 4 and we are operating on 8-bit numbers. Then
  // two_up(NUM_LOWER_BITS-1)   would be  00001000. This is our rounding point. If the number
  // is 00001001 then round up to 00010000. If it is 00000101 then round down to 00000000. If
  // it is 00001000 then round half to even comes into play. We must round to the nearest even number 
  // which is 00000000.
  if ( remainder > two_up(NUM_LOWER_BITS-1) ||
	  ( remainder == two_up(NUM_LOWER_BITS-1) && ((number+1)%2 == 0) ) )     // round half to even
      number += 1; 

  val = number << NUM_LOWER_BITS;
  
  if ( neg ) val = -val;

}

// Make the bottom NUM_LOWER_BITS bits all 0
void LongInt::trunc() {
  // If the number is -ve then work on the +ve version.
  bool neg = false;
   
  if ( val < 0 ) { 
    val = -val;
    neg = true;
  }

  // Get the number that is in the top bits of x
  long int number = val >> NUM_LOWER_BITS;
  
  val = number << NUM_LOWER_BITS;
  
  if ( neg ) val = -val;

}

// Multiply two LongInts which means multipy two long ints.
LongInt LongInt::operator*(const LongInt& li) {
  return LongInt(val*li.val);
}


// ------------ Utility functions ----------------------------------------------------------------------

// 2 to the power of "power"
long int two_up(int power) {
  long int x = 1;
  for (int i=0; i<power; ++i) x *= 2;
  return x;
}


// Find if an option to the program exists
bool find_option(int argc, char *argv[], const char *opt) {
  string opt_s = opt;
  for (int i=0; i<argc; ++i) {
    string argv_s = argv[i];
    if ( opt_s == argv_s ) return true;
  }
  return false;
}

// ------------ Main -----------------------------------------------------------------------------------
  
  
int main(int argc, char *argv[]) {

  // Print program options
  for (int i=1; i<argc; ++i) cout << argv[i] << " ";
  cout << endl;

  double user_scale = atof(argv[1]);
  int num_samples = atoi(argv[2]);
  
  ArrayFloat gains, bandpass;
  
  default_random_engine generator;
  normal_distribution<double> random(0.0, 1.0);
  
  // Generate the gains and bandpass, based on the options specified.  
  if ( find_option(argc, argv, "gains:flat") ) {
	ArrayFloat solved("/home/hgarsden/tmp/gains_solved.dat");   // only want the length to generate flat line gains
    gains.flat_gains(solved.len);
    bandpass.bp_setup(solved);
    
  } else if ( find_option(argc, argv, "gains:lines") ) {       // Gains have been made into crude straight lines
    gains.load_gains("/home/hgarsden/tmp/gains_solved_lines.dat");
    bandpass.bp_setup(gains);
    
  } else if ( find_option(argc, argv, "gains:curved") ) {        // Original gains that are curved like the bandpass in real data
    gains.load_gains("/home/hgarsden/tmp/gains_solved.dat");
    bandpass.bp_setup(gains);
  } else {
    cerr << "No gains specification\n";
    return 1;
  }
  
  // It might be interesting to flip the bandpass so that it trends in the same 
  // direction as the gains rather than inverted from the gains.
  if ( find_option(argc, argv, "bandpass_flip:on") ) 
    bandpass.flip();
  if ( find_option(argc, argv, "bandpass_flat:on") ) bandpass.flat();

  
  int saturation_positive=0;
  int saturation_negative=0;
  int saturation_check=0;

  ArrayInt64 signal_equiv(bandpass.len);			
  ArrayFloat signal_equivf(bandpass.len);  

  double max_raw = -1e39;
            
  // Find out what the values are like, so can scale. I want to fit the values into the
  // 4 bits as nicely as possible, so do a test rune first to see what the values are like
  // and generate a scale. Won't be perfect.
  for (int j=0; j<10; ++j) {
    for (int i=0; i<bandpass.len; ++i) { 
    
      double val_raw = bandpass[i]*random(generator)*gains[i];
      if ( val_raw > max_raw ) max_raw = val_raw;
    }
  }
  double scale = two_up(NUM_LOWER_BITS+3)/max_raw;
  cout << "Scale " <<  scale << endl; 
  
  bandpass.scale(scale);
  gains.savetxt("gains.dat");
  bandpass.savetxt("bandpass.dat");
  
  cout << "Bandpass mean: " << bandpass.mean() << endl;
  cout << "Gains mean: " << gains.mean() << endl;
  
  max_raw = -1e39;
  double min_raw = 1e39;
  double max_sat = max_raw;
  double min_sat = min_raw;
  double flipper = 0.3;		// flip this +ve/-ve as sudo random around 0
  bool do_flip = find_option(argc, argv, "random:flip");
  bool saturation_check_on = find_option(argc, argv, "saturation_check:on");
  bool rounding_on = find_option(argc, argv, "rounding:on");

  // The loop that does it

  for (int j=0; j<num_samples; ++j) {			// This loop is over time. Generate many values
            
    for (int i=0; i<gains.len; ++i) {			// This loop is across the band i.e.channels.
      double val_raw;
    	 
      if ( do_flip ) {   // Instead of Gauss random variables around 0, just generate +X, -X alternately.
    	val_raw = bandpass[i]*flipper*user_scale;
    	flipper *= -1;
      } else
        val_raw = bandpass[i]*random(generator)*user_scale;		// Make sure is a high valued integer for resolution
      if ( val_raw > max_raw ) max_raw = val_raw;
      if ( val_raw < min_raw ) min_raw = val_raw;
      
      // Now we get to the meat of the quantization. The operations are simple. Many intermediate values
      // are saved for reporting.
    

      // Larry: Multiply by an unsigned 32b coefficient ("digital gain") producing a signed 50b product.
      LongInt val = LongInt(val_raw)*LongInt(gains[i]);
    
           
      //val = LongInt(0b0000000000000000000000000000010111010101011101000000010001110110);

      // Larry: Select bits 35:32 (of 49:0), rounding symmetrically using the value of bit 31.  Danny:  34:31
      LongInt val_round = val;
      if ( rounding_on ) val_round.round_even();
      else val_round.trunc();     
      LongInt val_result = val_round; 
      val_result.right_shift(NUM_LOWER_BITS);			// Select the number. Down shift preserves negative. Leave top bits for saturation check.
      
      //cout << i << " " << val_result.val << " " << LongInt(val_raw).val << " " << LongInt(gains[i]).val << " " << LongInt(val).binary_str() << " " << setprecision(15) << val_raw*gains[i]/two_up(NUM_LOWER_BITS) << endl;
    
       
      LongInt val_sat = val_result;         // save value before saturation
      if ( val_sat.val > max_sat ) max_sat = val_sat.val;
      if ( val_sat.val < min_sat ) min_sat = val_sat.val;
    

      // Saturation: (Larry) If bits 49:35 are not all the same, force the 4b result to 0b0111 (+7) if it is positive and to 0b1001 (-7) if it is negative.
      // If the result is now 0b1000 (-8), force it to 0b1001 (-7). 
      if ( saturation_check_on ) {
        ++saturation_check;
        if  ( val_result.val < -7 || val_result.val > 7 ) {
          //cout << "Saturation ";
          if ( val_result.val > 7 ) { 
            //cout << "positive\n";
            val_result.val = 7;
            ++saturation_positive;
          } else { 
            //cout << "negative\n";
            ++saturation_negative;
            val_result.val = -7;
          }
        }
      }
                
    
      /*cout << "Orig Val (int x int)  \n" << val.binary_str() << " (" << val.val << ")" << endl;
      string shadow;
      for (int si=63; si>=0; --si) 
       if ( si >= NUM_LOWER_BITS && si <= NUM_LOWER_BITS+3  ) shadow += "*";
       else shadow += " ";            
      cout << shadow << endl;
      cout << "After rounding\n" << val_round.binary_str() << endl;
      cout << "Val after shift/after saturation remove\n" << val_sat.binary_str() << "\n" <<  val_result.binary_str() << endl;
      cout << "Res " << val_result.val << endl;
      */
      
      
      // This is the final result. An array of integrated squared quantized values. Squared so that they are like power.
      signal_equiv[i] += val_result.val*val_result.val;

	      // Float version. This shows what high precision would do.
      double valf = (val_raw*gains[i])/two_up(NUM_LOWER_BITS);
      signal_equivf[i] += valf*valf;

  
    }
    
    if ( j%10000 == 0 && j > 0 ) {		// Print information
      cout << "Wrote " << j << endl;
      char name[64];
      sprintf(name, "signal_equiv_%d.dat", j);
      signal_equiv.savetxt(name);
      sprintf(name, "signal_equivf_%d.dat", j);
      signal_equivf.savetxt(name);

      cout << "Saturation positive " << saturation_positive << " Saturation negative " << saturation_negative << endl;
      cout << "Bandpass range after scaling " << min_raw << " " << max_raw << endl;
      cout << "Equalized data range " << min_sat << " " << max_sat << endl;
    }
  }
  
  char name[64];
  sprintf(name, "signal_equiv.dat");
  signal_equiv.savetxt(name);
  sprintf(name, "signal_equivf.dat");
  signal_equivf.savetxt(name);
  cout << "Saturation positive " << saturation_positive << " Saturation negative " << saturation_negative << " checked " << saturation_check << endl;
  cout << "Data range " << min_raw << " " <<  max_raw << endl;

}
